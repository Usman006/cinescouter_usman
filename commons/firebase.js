// import * as firebase from 'firebase';
const firebase = require("firebase");
require("firebase/firestore");

const firebaseConfig = {
    // apiKey: "AIzaSyBpzu9v4JfGgxd74KVqaMl4EQaeSK7Eslo",
    // authDomain: "thestoryapp-4cd63.firebaseapp.com",
    // databaseURL: "https://thestoryapp-4cd63.firebaseio.com",
    // projectId: "thestoryapp-4cd63",
    // storageBucket: "thestoryapp-4cd63.appspot.com",
    // messagingSenderId: "162948460751"    
    apiKey: "AIzaSyDXgQrU85rMoR5OAvzafuMqoUpyFmh2Cm0",
    authDomain: "cinescouter-b5192.firebaseapp.com",
    databaseURL: "https://cinescouter-b5192.firebaseio.com",
    projectId: "cinescouter-b5192",
    storageBucket: "cinescouter-b5192.appspot.com",
    messagingSenderId: "571408496057",
    appId: "1:571408496057:web:6123786268253cc3"
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
};

// console.log('firebase 2');
// let app = firebase.initializeApp(firebaseConfig);
// export const firebaseApp = app.database();