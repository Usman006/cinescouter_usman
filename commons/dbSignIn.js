import * as firebase from 'firebase';
import { connect } from 'react-redux';
import { editProfilo } from '../reducers/actions';

export const createUser2 = (email, pass)  => {
    console.log(email);
    console.log(pass);
    firebase.auth().createUserWithEmailAndPassword(email, pass)
        .then((resp) => {
            this.props.setUIToken(resp.user.uid);
            // console.log(resp.user.uid);
        })
        .catch((error) => {
            console.log('error ', error)
        })      
}    
