import { AsyncStorage } from 'react-native';
import { firebaseApp }  from './firebase';

export const edtProfilo = (profilo, userToken) => {
    console.log(userToken)
    const ref = firebaseApp.ref('UsersList/' + userToken).update({
        sPathImmagine: profilo.sPathImmagine,
        sNome: profilo.sNome,
        sCognome: profilo.sCognome,
        sMotto: profilo.sMotto,
        iInteresse: profilo.iInteresse,
        sInteresse: profilo.sInteresse
    });
    ref.then(() => {

    }).catch((error) => {
        console.log('error ', error)
    })
}

export const newProfilo = (profilo, ui) => {
    const ref = firebaseApp.ref('UsersList/' + ui).push({
        sPathImmagine: profilo.sPathImmagine,
        sNome: profilo.sNome,
        sCognome: profilo.sCognome,
        sMotto: profilo.sMotto,
        iInteresse: profilo.iInteresse,
        sInteresse: profilo.sInteresse
    });
    ref.then(() => {
        AsyncStorage.setItem('userToken2', ref.key);
    }).catch((error) => {
        console.log('error ', error)
    })
    return ref.key;
}

export const getProfilo = (userToken) => {
    firebaseApp.ref('UsersList/' + userToken).once('value')
        .then(function (snapshot) {
            console.log('Nome ' + snapshot.val().sNome);
            return ('jj');
            let new_data = snapshot.val();
        });
} 