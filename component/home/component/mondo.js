import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'

export default class mondo extends Component {
  render() {
    
    const {navigation} = this.props.navigation.state.params;

    return (
      <View>
          <TouchableOpacity onPress={() => { navigation.navigate('ProgettoDettaglio') }}>
            <Text> mondo </Text> 
          </TouchableOpacity>
      </View>
    )
  }
}
