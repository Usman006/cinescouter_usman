import React, { Component } from 'react';
import { View } from 'react-native';
import { createAppContainer, createMaterialTopTabNavigator } from 'react-navigation';
import TabHome from "../component/home";
import TabMondo from '../component/mondo';

export default class mainHome extends Component {

  render() {

    const AppNavigator = createMaterialTopTabNavigator({
      Home: {
        screen: TabHome,
        params: { navigation: this.props.navigation },      
      },
      Mondo: {
        screen: TabMondo,
        params: { navigation: this.props.navigation }
      },
    }, {
        tabBarOptions: {
          activeTintColor: '#fff',
          inactiveTintColor: '#000',
          style: {
            backgroundColor: '#fff',
            height: 40,
          },
          indicatorStyle: {
            backgroundColor: '#000',
          },
          labelStyle: {
            fontSize: 18,
            fontFamily: 'Lovelo',
            top: -5
          },
          indicatorStyle: {
            height: null,
            top: 0,
            backgroundColor: '#000',
          },
        }
      });

    const AppIndex = createAppContainer(AppNavigator)

    return (
      <View style={{ flex: 1 }}>
        <AppIndex />
      </View >
    );
  }
}
