import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'

export default class CercaScreen extends Component {
  render() {
    return (
      <View>
          <TouchableOpacity onPress={() => { this.props.navigation.navigate('DettaglioScheda'); }}>
            <Text> Cerca Film </Text> 
          </TouchableOpacity>
      </View>
    )
  }
}
