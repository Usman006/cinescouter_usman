import { connect } from 'react-redux';
import { setUserToken, editProfilo } from '../../../reducers/actions';
import CercaScreen  from "../component/cerca";

function mapStateToProps(state) {
    console.log(state);
    return {
        ui: state.ui,     
    }    
}

function mapDispatchToProps(dispatch) {
    return {  
        setUIToken: (userToken) => {
            dispatch(setUserToken(userToken));
        },             
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CercaScreen);

