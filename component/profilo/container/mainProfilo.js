import { connect } from 'react-redux';
import { editProfilo } from '../../../reducers/actions';
import Profilo  from "../component/profilo";

function mapStateToProps(state) {
    console.log(state);
    return {
        profilo: state.profilo,
        ui: state.ui,        
    }    
}

function mapDispatchToProps(dispatch) {
    return {
        onInputChange: (profilo) => {
            dispatch(editProfilo(profilo));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profilo);
