import React, { Component } from 'react'
import { TouchableOpacity, View, Image } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import { createBottomTabNavigator } from 'react-navigation-tabs';

export default class footer extends Component {
  render() {
    return (
        <LinearGradient
          colors={["#fa5a7a", "#fa057a", "#e30009"]}
          start={{ x: 2, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{flex: .1}}
        >
          <View style={{ flex:1 , flexDirection: 'row',alignItems: 'center' }}>
            <TouchableOpacity style={{ flex: 1, alignItems: 'flex-start' }}>
              <Image source={require('../../../assets/img/cerca-bianca.png')} style={{ width: 25, height: 25, marginLeft: 25 }} />
            </TouchableOpacity>
            <TouchableOpacity style={{ flex: 1, alignItems: 'center' }}> 
              <Image source={require('../../../assets/img/fab-bianca.png')} style={{ width: 25, height: 25 }} />
            </TouchableOpacity>
            <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end' }}>
              <Image source={require('../../../assets/img/profilo-bianca.png')} style={{ width: 25, height: 25, marginRight: 25 }} />
            </TouchableOpacity>
            <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end' }}>
              <Image source={require('../../../assets/img/alert.png')} style={{ tintColor: '#fff', width: 33, height: 33, marginRight: 25 }} />
            </TouchableOpacity>
          </View>
        </LinearGradient>
    )
  }
}
