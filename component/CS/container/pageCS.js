import { connect } from 'react-redux';
import { setUserToken, editProfilo } from '../../../reducers/actions';
import mainCS from "../screen/mainCS"

const mapStateToProps = state => ({
    ui: state.ui,
})

const mapDispatchToProps = ({
    setUserToken
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(mainCS);

