import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'
import CardConcept from "../component/cardConcept"

export default class concept extends Component {
  
  _goDetail = (obj) => {
    const { navigation } = this.props.navigation.state.params;
    
    navigation.navigate('ConceptDettaglio', { navigation: navigation, obj: obj }) 
    return true
  }

  render() {

    return (
      <View>
        {/* <TouchableOpacity onPress={() => { navigation.navigate('ProgettoDettaglio', { navigation: navigation, txt: "progetto" }) }}>
          <Text> Progetti </Text>
        </TouchableOpacity> */}
        <ScrollView>
          <CardConcept
            imgBack={require("../../../../assets/imgCine/Insomnia.jpg")}
            txtTitolo={"Insomnia"}
            txtGenere={"Fantascienza"}
            txtCitazione={"Noi non dormiamo"}
            onDetail={this._goDetail}
          />
          {/* <CardConcept
            imgBack={require("../../../../assets/imgCine/anon.jpeg")}
            txtTitolo={"Anon"}
            txtGenere={"Azione - Fantascienza"}
            txtCitazione={"Ether ci controlla"}
            onDetail={this._goDetail}
          /> */}
        </ScrollView>
      </View>
    )
  }
}
