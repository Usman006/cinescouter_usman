import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, ImageBackground , FlatList ,Platform} from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import { Container, Header, Content, Card, CardItem, Body } from "native-base";
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';




const DATA = [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: 'First Item',
    },
    {
      id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
      title: 'Second Item',
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d72',
      title: 'Third Item',
    },
  ];



export default class progetto extends Component {

    componentDidMount(){        

    }

    // _onDetail = () => {
    //     const { imgBack, txtTitolo, txtGenere, txtCitazione } = this.props

    //     const obj = {
    //         imgBack: imgBack,
    //         txtTitolo: txtTitolo,
    //         txtGenere: txtGenere,
    //         txtCitazione: txtCitazione
    //     }

    //     this.props.onDetail(obj)
    // }

    render() {        
        return (
            <View style={styles.container}>
                  <FlatList
                   data={DATA}
                   renderItem={({ item }) => <Item Name={item.Name} />}
                   keyExtractor={item => item.id}
                   />             
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
      avatar: {
        width: 50,
        height: 50,
        borderRadius:50/2,
        borderWidth: 0.5,
        borderColor: "black",
      },
      bottombar:{
        flexDirection:'row',
        alignSelf:'center',
        marginHorizontal:20
      },
      bottombarText:{
        color:'black',
        fontSize:10,
        marginHorizontal:5
      },
      bottomCard:{
        flex:1,
        flexDirection:'row',
        alignSelf:'center'
      },
      bottomCardText:{
        color:'#E40E50',
        fontSize:14
      },
      bottomCardIcon:{
        color: '#E40E50', 
        marginHorizontal:4 
      },
      cardItem:{
        flexDirection:'row',
        flex:1 ,
        alignItems:'center' ,
        justifyContent:'center',
        padding:10,
        backgroundColor:'transparent'
      }
})


function Item({ Name }) {
  return (
    <View style={{flex:1}}>
       
        <Card>
          <CardItem header style={styles.cardItem}>
          
            <Image style={styles.avatar}
                source={{uri: 'https://bootdey.com/img/Content/avatar/avatar6.png'}}/>
                
                <View style={{flex:5,margin:10,marginTop:0}}>
                    <Text style={{fontWeight:'800',fontSize:16 }}>First Name</Text>
                    <Text style={{marginTop:-5}}>Second Name</Text>
                </View>

          </CardItem>
          <ImageBackground 
              style={{width:'100%',height:300,marginTop:-10}}
              source={{uri: 'https://www.dominionpost.com/wp-content/uploads/2018/09/Photo-Friday-6-15.jpg'}}>                
            <Body style={{backgroundColor: 'rgba(0,0,0, 0.1 )'}}>
              <View style={{position:'absolute',bottom:0, margin:10}}>
               <Text style={{fontWeight:'normal',color:'white' ,fontSize:20}}>
                   Helsinki Rome
                   </Text> 
                   <Text style={{fontWeight:'normal',color:'white' ,fontSize:16}}>
                   Horror
                   </Text>   
              <Text style={{color:'white',fontSize:12}}>
                NativeBase is a free and open source framework that enable
                developers to build
                high-quality mobile apps using React Native iOS and Android
                apps
                with a fusion of ES6.
              </Text>
              </View>
            </Body>
         
          </ImageBackground>
              <View style={{flexDirection:'row',height:20 ,backgroundColor:'lightgrey'}}>
                      <View style={styles.bottombar}>
                      <Entypo
                       name="eye"
                       style={{ color: 'black' }}
                       size={Platform.OS === "ios" ? 18 : 15}
                      />
                         <Text style={styles.bottombarText}>51</Text>
                      </View>

                      <View style={styles.bottombar}>
                      <FontAwesome5
                       name="arrow-alt-circle-up"
                       style={{ color: 'black' }}
                       size={Platform.OS === "ios" ? 18 : 15}
                      />
                         <Text style={styles.bottombarText}>4</Text>
                      </View>
                        
                      <View style={styles.bottombar}>
                      <FontAwesome
                       name="commenting-o"
                       style={{ color: 'black' }}
                       size={Platform.OS === "ios" ? 18 : 15}
                      />
                         <Text style={styles.bottombarText}>commenti</Text>
                      </View>
              </View>
          <CardItem footer bordered style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
           
          <TouchableOpacity  onPress={()=>console.log('You Press Consiglia')}
          style={styles.bottomCard}>
          <FontAwesome5
                       name="arrow-alt-circle-up"
                       style={styles.bottomCardIcon}
                       size={Platform.OS === "ios" ? 28 : 20}
                      />
                         <Text style={styles.bottomCardText}>Consiglia</Text>
            </TouchableOpacity>

            
            <TouchableOpacity onPress={()=>console.log('You Press Commenta')}
            style={styles.bottomCard}>
            <FontAwesome
                       name="commenting-o"
                       style={styles.bottomCardIcon}
                       size={Platform.OS === "ios" ? 28 : 20}
                      />
                         <Text style={styles.bottomCardText}>Commenta</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={()=>console.log('You Press Preferiti')}
            style={styles.bottomCard} >
            <FontAwesome
                       name="heart-o"
                       style={styles.bottomCardIcon}
                       size={Platform.OS === "ios" ? 28 : 20}
                      />
                       <Text style={styles.bottomCardText}>Preferiti</Text>
            </TouchableOpacity>

          </CardItem>
        </Card>
    </View>
  );
}