import React, { Component } from 'react'
import { Image, View, TouchableOpacity, Text, StyleSheet, ImageBackground } from 'react-native'
import { Thumbnail } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import Footer from "../../../components/footer/footer"
class BtnSegui extends Component {
  render() {
    return (
      <View style={{ width: 60, height: 25, backgroundColor: "#ffff", borderRadius: 5, top: 7, alignItems: "center", justifyContent: "center" }}>
        <Text style={styles.txtSegui}>Segui</Text>
      </View>
    )
  }
}
export default class conceptDettaglio extends Component {

  _goBack = () => {
    this.props.navigation.goBack();
    return true
  }

  componentDidMount() {

  }

  render() {

    const { obj } = this.props.navigation.state.params;

    return (
      <View style={styles.container} >
        <LinearGradient
          colors={["#fa5a7a", "#fa057a", "#e30009"]}
          start={{ x: 2, y: 0 }}
          end={{ x: 0, y: 0.5 }}
        >
          <View style={{ height: 100, alignItems: 'center', flexDirection: 'column' }}>
            <Image source={require('../../../../assets/img/Cinescouter-bianca.png')} style={{ width: 180, height: 36, top: 20 }} />
            <TouchableOpacity onPress={this._goBack} style={{ alignItems: "flex-start", justifyContent: "flex-start", flex: 1, width: "100%" }}>
              <Image source={require('../../../../assets/img/back.png')} style={{ tintColor: '#fff', width: 35, height: 35, marginTop: 15, marginLeft: 15 }} />
            </TouchableOpacity>
          </View>
        </LinearGradient>

        <View style={{ flex: 1 }}>
          <ImageBackground source={obj.imgBack} style={styles.imgBack}>
            <View style={{ bottom: 5, marginHorizontal: 15 }}>
              <Text style={styles.txtTitolo}>{obj.txtTitolo}</Text>
              <Text style={styles.txtGenere}>{obj.txtGenere}</Text>
              <Text style={styles.txtCitazione}>"{obj.txtCitazione}"</Text>
            </View>
          </ImageBackground>
          <View style={{ flex: 1.8 }}>
            <View style={{ height: 40 }}>
              <LinearGradient
                colors={["#fa5a7a", "#fa057a", "#e30009"]}
                start={{ x: 2, y: 0 }}
                end={{ x: 0, y: 1 }}
                style={{ flex: 1, justifyContent: "center", flexDirection: "row" }}
              >
                <View style={{ flex: 1.3, flexDirection: "row", justifyContent: 'flex-end', marginRight: 15 }}>
                  <BtnSegui />
                  <Image source={require('../../../../assets/img/up.png')} style={{ width: 26, height: 26, marginTop: 5, marginHorizontal: 10 }} />
                  <Text style={styles.txtUP}>1600</Text>
                </View>
              </LinearGradient>
            </View>
            <View style={{ height: 80, flexDirection: "row", alignItems: "center", }}>
              <View style={{ flex: 1 }}>
                <View style={styles.viewPicture}>
                  <Thumbnail large style={styles.tmbProfile} source={require('../../../../assets/img/user.png')} />
                </View>
                <Text style={styles.txtUtente}>Dino Imbriani</Text>
              </View>
              <View style={{ flex: 2, alignItems: "flex-end" }}>
                <View style={styles.vwTransform}>
                  <Text style={styles.txtTransform}>Trasforma in progetto</Text>
                </View>
              </View>
            </View>
            <View style={{ height: 40 }}>
              <LinearGradient
                colors={["#fa5a7a", "#fa057a", "#e30009"]}
                start={{ x: 2, y: 0 }}
                end={{ x: 0, y: 1 }}
                style={{ flex: 1, justifyContent: "center", flexDirection: "row" }}
              >
                <View style={{ flex: 1.3, flexDirection: "row", justifyContent: 'center' }}>
                  <Text style={styles.txtUP}>Concept</Text>
                </View>
              </LinearGradient>
            </View>
          </View>
        </View>

        <Footer />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flexDirection: 'column',
    flex: 1
  },
  imgBack: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
  },
  txtTitolo: {
    fontSize: 18,
    fontFamily: 'Lovelo',
    color: "#fff",

  },
  txtGenere: {
    fontSize: 11,
    fontFamily: 'Lovelo',
    color: "#fff"
  },
  txtCitazione: {
    fontSize: 12,
    fontFamily: 'Helvetica Bold',
    color: "#fff"
  },
  txtSegui: {
    fontSize: 13,
    fontFamily: 'Lovelo',
    color: "#000"
  },
  txtUP: {
    fontSize: 18,
    fontFamily: 'Lovelo',
    color: "#fff",
    alignSelf: "center"
  },
  txtUtente: {
    fontSize: 12,
    fontFamily: 'Lovelo',
    color: "#000",
    alignSelf: "flex-start",
    marginLeft: 15
  },
  viewPicture: {
    width: 86,
    height: 86,
    backgroundColor: '#ffffff',
    borderRadius: 100,
    marginTop: -25,
    marginLeft: 15
  },
  tmbProfile: {
    margin: 3,
    borderWidth: 1,
    borderColor: "#000"
  },
  vwTransform: {
    backgroundColor: "#000",
    height: 40,
    width: 200,
    borderRadius: 5,
    marginRight: 25,
    justifyContent: "center"
  },
  txtTransform: {
    fontSize: 12,
    fontFamily: 'Lovelo',
    color: "#fff",
    alignSelf: "center"
  },
})
