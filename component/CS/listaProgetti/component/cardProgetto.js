import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, ImageBackground } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';

class BtnSegui extends Component {
    render() {
        return (
            <View style={{ width: 60, height: 25, backgroundColor: "#ffff", borderRadius: 5, top: 7, alignItems: "center", justifyContent: "center" }}>
                <Text style={styles.txtSegui}>Segui</Text>
            </View>
        )
    }
}
export default class progetto extends Component {
    render() {

        const { imgBack, txtTitolo, txtGenere, txtCitazione } = this.props
        
        return (
            <View style={{ marginHorizontal: 20, height: 250, borderRadius: 5, borderWidth: 1, marginVertical: 10 }}>
                <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.onDetail()}>
                    <ImageBackground source={imgBack} style={styles.imgBack}>
                        <View style={{bottom: 5, marginHorizontal: 10}}>
                            <Text style={styles.txtTitolo}>{txtTitolo}</Text>
                            <Text style={styles.txtGenere}>{txtGenere}</Text>
                            <Text style={styles.txtCitazione}>"{txtCitazione}"</Text>
                        </View>
                    </ImageBackground>
                    {/* <View>
                        <Text> textInComponent </Text>
                    </View> */}
                </TouchableOpacity>

                <View style={{ height: 40 }}>
                    <LinearGradient
                        colors={["#fa5a7a", "#fa057a", "#e30009"]}
                        start={{ x: 2, y: 0 }}
                        end={{ x: 0, y: 1 }}
                        style={{ flex: 1, justifyContent: "center", flexDirection: "row" }}
                    >
                        <View style={{ flex: 1 }}>
                            <Image source={require('../../../../assets/img/occhio.png')} style={{ tintColor: '#000', width: 26, height: 26, marginTop: 5, marginLeft: 15 }} />
                        </View>
                        <View style={{ flex: 1.3, flexDirection: "row" }}>
                            <Image source={require('../../../../assets/img/commenti.png')} style={{ width: 30, height: 30, marginTop: 5, marginHorizontal: 10 }} />
                            <BtnSegui />
                            <Image source={require('../../../../assets/img/up.png')} style={{ width: 26, height: 26, marginTop: 5, marginHorizontal: 10 }} />
                            <Text style={styles.txtUP}>1600</Text>
                        </View>


                    </LinearGradient>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F5FCFF',
        flexDirection: 'column',
        flex: 1
    },
    imgBack: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
    },
    txtTitolo: {
        fontSize: 18,
        fontFamily: 'Lovelo',
        color: "#fff"
    },
    txtGenere: {
        fontSize: 11,
        fontFamily: 'Lovelo',
        color: "#fff"
    },
    txtCitazione: {
        fontSize: 12,
        fontFamily: 'Helvetica Bold',
        color: "#fff"
    },
    txtUP: {
        fontSize: 18,
        fontFamily: 'Lovelo',
        color: "#fff",
        alignSelf: "center"
    },
    txtSegui: {
        fontSize: 13,
        fontFamily: 'Lovelo',
        color: "#000"
    }
})
