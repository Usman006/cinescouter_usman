import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'
import CardProgetto from "../component/cardProgetto"

export default class progetto extends Component {

  _goDetail = () => {
    const { navigation } = this.props.navigation.state.params;

    navigation.navigate('ProgettoDettaglio', { navigation: navigation }) 
    return true
  }

  render() {    

    return (
      <View>
        {/* <TouchableOpacity onPress={() => { navigation.navigate('ProgettoDettaglio', { navigation: navigation, txt: "progetto" }) }}>
          <Text> Progetti </Text>
        </TouchableOpacity> */}
        <ScrollView>
          <CardProgetto
            imgBack={require("../../../../assets/imgCine/inception.jpg")}
            txtTitolo={"Inception"}
            txtGenere={"Azione - Fantascienza"}
            txtCitazione={"La tua mente è la scena del crimine"}
            onDetail={this._goDetail}
          />
          <CardProgetto
            imgBack={require("../../../../assets/imgCine/anon.jpeg")}
            txtTitolo={"Anon"}
            txtGenere={"Azione - Fantascienza"}
            txtCitazione={"Ether ci controlla"}
            onDetail={this._goDetail}
          />
        </ScrollView>
      </View>
    )
  }
}
