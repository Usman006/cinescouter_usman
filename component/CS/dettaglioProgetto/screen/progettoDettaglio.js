import React, { Component } from 'react'
import { Image, View, TouchableOpacity, Text, StyleSheet } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import Footer from "../../../components/footer/footer"

export default class progettoDettaglio extends Component {
  
  _goBack = () => {
    this.props.navigation.goBack();
    return true
  }

  render() {
    
    const { navigation } = this.props

    return (
      <View style={styles.container} >
        <LinearGradient
          colors={["#fa5a7a", "#fa057a", "#e30009"]}
          start={{ x: 2, y: 0 }}
          end={{ x: 0, y: 0.5 }}
        >
          <View style={{ height: 100, alignItems: 'center', flexDirection: 'column' }}>
            <Image source={require('../../../../assets/img/Cinescouter-bianca.png')} style={{ width: 180, height: 36, top: 20 }} />
            <TouchableOpacity onPress={this._goBack} style={{ alignItems: "flex-start", justifyContent: "flex-start", flex: 1, width: "100%" }}>
              <Image source={require('../../../../assets/img/back.png')} style={{ tintColor: '#fff', width: 35, height: 35, marginTop: 15, marginLeft: 15 }} />
            </TouchableOpacity>
          </View>
        </LinearGradient>

        <View style={{ flex: 1 }}>
          <Text>{navigation.state.params.txt}</Text>
        </View>

        <Footer />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flexDirection: 'column',
    flex: 1
  },
})
