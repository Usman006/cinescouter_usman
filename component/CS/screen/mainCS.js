import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { createAppContainer, createMaterialTopTabNavigator } from 'react-navigation';
import { Header, Body, Title } from 'native-base';
import TabConcept from "../listaConcept/screen/concept";
import TabProgetto from '../listaProgetti/screen/progetti';

export default class mainCS extends Component {

  render() {

      const AppNavigator = createMaterialTopTabNavigator({
        Concept: {
          screen: TabConcept,
          params: { navigation: this.props.navigation },      
        },
        Progetti: {
          screen: TabProgetto,
          params: { navigation: this.props.navigation },      
        },
      }, {
          tabBarOptions: {
            activeTintColor: '#fff',
            inactiveTintColor: '#000',
            style: {
              backgroundColor: '#fff',
              height: 40,
            },
            indicatorStyle: {
              backgroundColor: '#000',
            },
            labelStyle: {
              fontSize: 18,
              fontFamily: 'Lovelo',
              top: -5
            },
            indicatorStyle: {
              height: null,
              top: 0,
              backgroundColor: '#e30009',
            },      
          }
        });
  
      const AppIndex = createAppContainer(AppNavigator)

    return (
        <View style={{ flex: 1 }}>
          <Header style={{backgroundColor:'white'}}>
          <Body style={{alignItems:'center',justifyContent:'center'}}>
            <Title style={{color:'black'}}>Scouting</Title>
          </Body>
        </Header>
          <AppIndex />
        </View>      
    );
  }
}