import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, KeyboardAvoidingView, ScrollView, AsyncStorage } from 'react-native';
import { Button, Icon, Container, Item, Input, Form, Label } from 'native-base';
import { createUser } from '../../../commons/dbSignIn'
import { connect } from 'react-redux';
import { setUserToken } from '../../../reducers/actions';
import * as firebase from 'firebase';

class SignInScreen extends React.Component {

    state = {
        email: '',
        password: ''
    }
    
    componentWillReceiveProps(nextProps) {
        console.log('next')
        console.log(nextProps.ui.userToken)
        if (nextProps.ui.userToken) {
                this.props.navigation.navigate('NewProfilo')
        } 
  }

    signIn = () => {               
        const {email, password} = this.state;
        // this.props.navigation.navigate('NewProfilo');
//        this.createUser(email, password);
    }

    createUser (email, pass) {
        firebase.auth().createUserWithEmailAndPassword(email, pass)
            .then((resp) => {
//                this.props.setUIToken(resp.user.uid);
            })
            .catch();
    }

    render() {
                return(
            <Container>
            <ScrollView contentContainerStyle={{ flexGrow: 1 }} scrollEnabled={false}>
                <KeyboardAvoidingView behavior="padding" style={{ flex: 1, bottom: 0 }}>
                    <ImageBackground source={require('../../../assets/img/BGLogin2.png')} style={styles.container}>
                    </ImageBackground>

                    <View style={styles.viewBG}>
                        <View style={styles.viewLogo}>
                            <Image source={require('../../../assets/img/Logo.png')} style={{ width: 100, height: 119 }} />
                            <Image source={require('../../../assets/img/Cinescouter-bianca.png')} style={{ width: 250, height: 50 }} />
                        </View>
                        <View style={styles.viewInput}>
                            <Form>
                                <Item floatingLabel style={styles.itmLogin}>
                                    <Label style={styles.lblItem}>E-Mail</Label>
                                    <Input  style={styles.inputItem} keyboardType='email-address' onChangeText={(newValue) => this.setState({ email: newValue })} />
                                </Item>
                                <Item floatingLabel style={styles.itmLogin}>
                                    <Label style={styles.lblItem}>Password</Label>
                                    <Input style={styles.inputItem} secureTextEntry={true} onChangeText={(newValue) => this.setState({ password: newValue })} />
                                </Item>
                                <Item floatingLabel style={styles.itmLogin}>
                                    <Label style={styles.lblItem}>Conferma Password</Label>
                                    <Input style={styles.inputItem} secureTextEntry={true} />
                                </Item>
                                <View style={styles.viewAccedi}>
                                    <Button style={styles.btnLogin} onPress={this.signIn}>
                                        <Text style={styles.txtBtnLogin} >Registrati!</Text>
                                    </Button>
                                    <Text style={styles.txtPwdDimenticata} >Non hai ricevuto l'e-mail di registrazione?</Text>
                                </View>
                            </Form>

                        </View>
                        <View style={styles.viewButtonSocial}>
                            <View>
                                <Button style={styles.btnSocial} onPress={this.signIn}>
                                    <Icon name='logo-google' />
                                    <Text style={styles.txtButton} >Accedi con Google</Text>
                                </Button>
                                <Button style={styles.btnSocial} onPress={this.signIn}>
                                    <Icon name='logo-facebook' />
                                    <Text style={styles.txtButton} >Accedi con Facebook</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                    <Icon name='md-close-circle' style={styles.iconExit} onPress={() => this.props.navigation.goBack()} />
                </KeyboardAvoidingView>
            </ScrollView>
            </Container >
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
    },
    viewBG: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(47,79,79, .6)',
        flexDirection: 'column',
    },
    viewLogo: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        margin: 15,
    },
    iconExit: {
        position: 'absolute',
        fontSize: 35,
        left: 10,
        top: 10,
        color: '#ffffff',
    },
    viewInput: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: 'rgb(255,255,255)',
        width: '100%',
    },
    viewButtonSocial: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 5,
        marginTop: 5,
    },
    txtButton: {
        fontSize: 15,
        fontFamily: 'Lovelo',
        color: '#fff',
        margin: 5,
        left: 2,
    },
    itmLogin: {
        margin: 5,
    },
    lblItem: {
        fontSize: 12,
        fontFamily: 'Lovelo',
    },
    inputItem: {
        fontSize: 15,
        fontFamily: 'Helvetica Bold',
    },
    btnLogin: {
        fontSize: 12,
        fontFamily: 'Lovelo',
        color: '#fff',
        width: 100,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#000',
        borderRadius: 10,             
    },
    txtBtnLogin: {
        fontSize: 15,
        fontFamily: 'Lovelo',
        color: '#fff',
        textAlign: 'center',
    },
    txtPwdDimenticata: {
        fontSize: 12,
        fontFamily: 'Lovelo',
        color: '#000000',
        textAlign: 'left',
        marginLeft: 10,
        maxWidth: 190,
    },
    viewAccedi: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    btnSocial: {
        width: 240,
        height: 53,
        margin: 8,
        backgroundColor: '#000',
        borderRadius: 10, 
        justifyContent: 'flex-start'         
    }
});

function mapStateToProps(state) {
    console.log(state);
    return {
        ui: state.ui,     
    }    
}

function mapDispatchToProps(dispatch) {
    return {  
        setUIToken: (userToken) => {
            dispatch(setUserToken(userToken));
        },             
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen);

