import { connect } from 'react-redux';
import { setUserToken, editProfilo } from '../../../reducers/actions';
import SignIn from "../component/signIn";

const mapStateToProps = state => ({
    ui: state.ui,
})

const mapDispatchToProps = ({
    setUserToken
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SignIn);

