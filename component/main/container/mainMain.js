import React, { Component } from 'react';
import { StyleSheet, View, Image, AsyncStorage, StatusBar, TouchableOpacity } from 'react-native';
import { Container, Tabs, Tab, TabHeading, Icon, Text,  Button, ScrollableTab, Title, Body, Left, Right } from 'native-base';
import TabCS from "../../CS/container/pageCS";
import TabHome from '../../home/container/mainHome';
import TabProfilo from '../../profilo/container/mainProfilo';
import TabMenu from '../../menu/container/mainMenu';
import Modal from 'react-native-modalbox';
import LinearGradient from 'react-native-linear-gradient';
import Footer from "../../components/footer/footer"

export default class HomeScreen extends Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      section: 1,
    };
    this.onPressBtnFabio = this.onPressBtnFabio.bind(this);
    this.SignUp = this.SignUp.bind(this);
  }

  onClose() {
    console.log('Modal just closed');
  }

  onOpen() {
    console.log('Modal just opened');
  }

  onClosingState(state) {
    console.log('the open/close of the swipeToClose just changed');
  }

  onPressBtnFabio(navTxt) {
    this.props.navigation.navigate(navTxt);
    this.refs.modFabio.close();
  }

  SignUp = async () => {
    AsyncStorage.clear()
    console.log('signup')
    this.props.navigation.navigate('AuthLoading')
  }

  setSection = (id) => {
    this.setState({ section: id });
  }

  render() {
    const {section} = this.state;
    // console.warn(this.props.navigation.navigate)       
    return (
      <View style={styles.container} >
        <StatusBar hidden={true} />

        <LinearGradient
          colors={["#fa5a7a", "#fa057a", "#e30009"]}
          start={{ x: 2, y: 0 }}
          end={{ x: 0, y: 0.5 }}
        >
          <View style={{ height: 135, alignItems: 'center', flexDirection: 'column' }}>
            <Image source={require('../../../assets/img/Cinescouter-bianca.png')} style={{width: 180, height: 36, top: 20 }} />
            <View style={{ flex: 1, top: 30, flexDirection: 'row' }}>
              <TouchableOpacity onPress={() => { this.setSection(1) }} style={{ flex: 1, alignItems: 'flex-start' }}>
                <Image source={require('../../../assets/img/home-bianca.png')} style={{ tintColor: section === 1 ? '#000' : '#fff', width: 25, height: 25, marginTop: 15, marginLeft: 25 }} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => { this.setSection(2) }} style={{ flex: 1, alignItems: 'center' }}>             
                <Image source={require('../../../assets/img/binocolo-bianca.png')} style={{ tintColor: section === 2 ? '#000' : '#fff', width: 25, height: 25, marginTop: 15 }} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => { this.setSection(3) }} style={{ flex: 1, alignItems: 'flex-end' }}> 
                <Image source={require('../../../assets/img/menù-bianca.png')} style={{tintColor: section === 3 ? '#000' : '#fff',  width: 25, height: 25, marginTop: 15, marginRight: 25 }} />
              </TouchableOpacity>
            </View>
          </View>
        </LinearGradient>
        <View style={{ flex: 1}}>
          {section == 1 && <TabHome navigation={this.props.navigation}/>}
          {section == 2 && <TabCS navigation={this.props.navigation}/>}
        </View>

        {/* <Footer/> */}
        
        {/* <LinearGradient
          colors={["#fa5a7a", "#fa057a", "#e30009"]}
          start={{ x: 2, y: 0 }}
          end={{ x: 0, y: 1 }}
        >
          <View style={{ height: 52, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity style={{ flex: 1, alignItems: 'flex-start' }}>
              <Image source={require('../../../assets/img/cerca-bianca.png')} style={{ width: 25, height: 25, marginLeft: 25 }} />
            </TouchableOpacity>
            <TouchableOpacity style={{ flex: 1, alignItems: 'center' }}> 
              <Image source={require('../../../assets/img/fab-bianca.png')} style={{ width: 25, height: 25 }} />
            </TouchableOpacity>
            <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end' }}>
              <Image source={require('../../../assets/img/profilo-bianca.png')} style={{ width: 25, height: 25, marginRight: 25 }} />
            </TouchableOpacity>
            <TouchableOpacity style={{ flex: 1, alignItems: 'flex-end' }}>
              <Image source={require('../../../assets/img/alert.png')} style={{ tintColor: '#fff', width: 33, height: 33, marginRight: 25 }} />
            </TouchableOpacity>
          </View>
        </LinearGradient> */}

        {/* <Footer>
          <FooterTab style={{ backgroundColor: '#F5F5F5' }}>
            <Button onPress={() => this.onPressBtnFabio('Cerca')}>
              <Icon name='ios-search' style={{ color: '#808080', fontSize: 30, }} />
            </Button>
            <Button onPress={() => this.refs.modFabio.open()}>
              <Icon name='ios-add-circle-outline' style={{ color: '#808080', fontSize: 30, }} />
            </Button>
          </FooterTab>
        </Footer> */}

        <Modal style={[styles.modal, styles.modFabio]} position={"bottom"} swipeToClose={false} ref={"modFabio"}>
          <View style={styles.divFabio}>
            <Button style={styles.btnFabio} onPress={() => this.onPressBtnFabio('Condividi')}>
              <Icon name='md-share' style={styles.iconText} />
              <Text style={styles.fabioText} uppercase={false}>Scrivi Post</Text>
            </Button>
            <Button style={styles.btnFabio} onPress={() => this.onPressBtnFabio('Trova')}>
              <Icon name='ios-help-circle' style={styles.iconText} />
              <Text style={styles.fabioText} uppercase={false}>Trova</Text>
            </Button>
            <Button style={styles.btnFabio} onPress={() => this.onPressBtnFabio('NuovaDicussione')}>
              <Icon name='ios-chatbubbles' style={styles.iconText} />
              <Text style={styles.fabioText} uppercase={false}>Apri Discussione</Text>
            </Button>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flexDirection: 'column',
    flex: 1
  },
  tabText: {
    fontFamily: 'GoogleSans-Regular',
  },
  tabTextTS: {
    fontSize: 35,
    fontFamily: 'GoogleSans-Bold',
  },
  left: {
    flex: 1,
  },
  body: {
    flex: 1,
  },
  bodyText: {
    fontSize: 25,
    fontFamily: 'GoogleSans-Bold',
    justifyContent: 'center',
    marginLeft: 5,
  },
  right: {
    flex: 1,
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  modFabio: {
    height: 50,
    bottom: 55
  },
  fabioText: {
    fontSize: 10,
    fontFamily: 'GoogleSans-Regular',
    color: '#808080',
  },
  iconText: {
    fontSize: 30,
    color: '#808080'
  },
  divFabio: {
    flex: 2,
    flexDirection: 'row',
    backgroundColor: '#F5F5F5',
  },
  btnFabio: {
    flex: 1,
    height: 55,
    flexDirection: 'column',
    margin: 2,
    backgroundColor: '#F5F5F5',
  },
});
