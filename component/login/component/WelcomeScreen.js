import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, TouchableOpacity, StatusBar } from 'react-native';
import { Button, Icon, Container, Item, Input, Form, Label } from 'native-base';

export default class Welcome extends React.Component {

    render() {
        return (
            <Container>
            <ImageBackground source={require('../../../assets/img/BGLogin2.png')} style={styles.container}>
                <StatusBar hidden={true} barStyle='light-content' backgroundColor="#1565c0" />
                <View style={styles.viewBG}>
                    <View style={styles.viewLogo}>
                        <Image source={require('../../../assets/img/Logo.png')} style={{ width: 130, height: 146 }} />
                        <Image source={require('../../../assets/img/Cinescouter-bianca.png')} style={{ width: 350, height: 70 }} />
                        <Text style={styles.txtClaim}>
                            "Dove nascono i Film e le Serie TV di domani"
                        </Text>                        
                    </View>
                    <View style={styles.viewButton}>
                        <Button style={styles.btnSocial} onPress={() => this.props.navigation.navigate('Login')}>
                            <Text style={styles.txtButton} >Accedi</Text>
                        </Button>
                        {/*<Button style={styles.btnLogin} title='Inizia' onPress={() => this.props.navigation.navigate('SignIn')}></Button>*/}
                        <TouchableOpacity onPress={() => {
                            this.props.navigation.navigate('SignIn');
                            }}>
                            <View style={{flexDirection: 'column'}}>
                                <Text style={styles.txtAccedi}>
                                    Non hai un account?
                                </Text>
                                <Text style={styles.txtAccedi}> Inizia!</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    viewBG: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'rgba(47,79,79, .6)',
        flexDirection: 'column',
    },
    viewLogo: {
        flex: 1,
        alignItems: 'center',
        // justifyContent: 'center',
        margin: 15,
        position: 'absolute'
    },
    viewTextClaim: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 20,
    },
    txtClaim: {
        fontSize: 12,
        fontFamily: 'Lovelo',
        color: '#fff',
        textAlign: 'center',
    },
    viewButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: 15,
    },
    btnSocial: {
        width: 200,
        height: 53,
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',  
        backgroundColor: '#000',
        borderRadius: 10,      
    },   
    txtButton: {
        fontSize: 25,
        fontFamily: 'Lovelo',
        color: '#fff',
        textAlign: 'center'
    },     
    btnLogin: {
        padding: 15,
        marginBottom: 8,
        width: 90,
        fontSize: 35,
        fontFamily: 'GoogleSans-Bold',
    },
    txtAccedi: {
        fontSize: 12,
        fontFamily: 'Lovelo',
        color: '#fff',
        textAlign: 'center',
    },
    txtAccedi2: {
        fontSize: 15,
        fontFamily: 'GoogleSans-Bold',
        color: '#fff',
        textAlign: 'center',
        margin: 15,
        marginLeft: 10,
    }
});
