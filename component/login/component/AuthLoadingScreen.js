import React, { Component } from 'react';
import { StyleSheet, StatusBar, View, ActivityIndicator, AsyncStorage, Image } from 'react-native';
import { connect } from 'react-redux';
import { setUserToken, editProfilo } from '../../../reducers/actions';
import { firebaseApp }  from '../../../commons/firebase';

class AuthLoadingScreen extends Component {

    constructor(){
        super();
        this.loadApp()
    }

    loadApp = async () => {
        console.log('load');
        await AsyncStorage.getItem('userToken').then((result)=> {            
            if (result) {                
                console.log('getuser');
                console.log(result);
                // this.getProfilo(result);
//                this.props.setUIToken(result);
            }
            this.props.navigation.navigate(result ? 'App' : 'Auth');
        })
    }

    // getProfilo = (userToken) => {  
    //     firebaseApp.ref('UsersList/' + userToken).once('value')
    //     .then((snapshot) => {
    //         const res = snapshot.val();
    //         const profilo = {                
    //             sPathImmagine: res.sPathImmagine,
    //             sNome: res.sNome,
    //             sCognome: res.sCognome,
    //             sMotto: res.sMotto,
    //             iInteresse: res.iInteresse,
    //             sInteresse: res.sInteresse,
    //         }
    //         this.props.onInputChange(profilo);            
    //     }) 
    // } 

    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle='light-content' backgroundColor="#1565c0" />
                <Image source={require('../../../assets/img/Logo.png')} style={{ width: 150, height: 178, marginBottom: 20 }} />
                <ActivityIndicator size="large" />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

function mapStateToProps(state) {
    return {
        profilo: state.profilo,
        ui: state.ui,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setUIToken: (userToken) => {
            dispatch(setUserToken(userToken));
        },
        onInputChange: (profilo) => {
            dispatch(editProfilo(profilo));
        },        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthLoadingScreen);
