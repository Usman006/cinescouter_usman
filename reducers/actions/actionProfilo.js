import { EDT_PROFILO, EDT_SINGLE_PROFILO } from './../types';

export const editProfilo =(profilo) => {
    return {
        type: EDT_PROFILO,
        payload: { profilo }
    }    
}

export const editSingleProfilo =(value, name) => {
    return {
        type: EDT_SINGLE_PROFILO,
        payload: { value, name }
    }    
}
