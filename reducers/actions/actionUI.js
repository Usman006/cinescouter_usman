import { SET_USERTOKEN } from './../types';

export const setUserToken =(userToken) => {
    return {
        type: SET_USERTOKEN,
        payload: userToken
    }    
}
