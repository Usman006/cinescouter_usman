const initialState = {
    profilo: {
        sPathImmagine: '',
        sNome: '',
        sCognome: '',
        sMotto: '',
        iInteresse: 0,
        sInteresse: '',
        iAuth: '',
    },
    ui: {
        userToken: true
    }
}

export default initialState;