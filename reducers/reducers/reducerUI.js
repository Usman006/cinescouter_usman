import initialState from './initialState';
import { SET_USERTOKEN } from './../types';

export default (state = initialState.ui, action) => {
    console.log('Action ');
    console.log(action.payload);
    switch (action.type) {
        case SET_USERTOKEN:
            state = Object.assign({}, state, {
                userToken: action.payload,
            });
            break;
    }
    return state;
}

