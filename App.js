import React from 'react';
import AuthLoadingScreen from './component/login/component/AuthLoadingScreen';
import WelcomeScreen from './component/login/component/WelcomeScreen';

// SIGN IN
import SignInScreen from './component/signIn/container/mainSignIn';
import NewProfiloScreen from './component/signIn/component/NewProfiloScreen';
import SelFilmScreen from './component/signIn/component/selFilm';
import SelUtentiScreen from './component/signIn/component/selUtenti';
import SelTipoProfiloScreen from './component/signIn/component/selTipoProfilo';

// LOGIN
import LoginScreen from './component/login/component/LoginScreen';

// CS
import CSScreen from './component/CS/container/pageCS';
import ConceptDettaglioScreen from './component/CS/dettaglioConcept/screen/conceptDettaglio';
import ProgettoDettaglioScreen from './component/CS/dettaglioProgetto/screen/progettoDettaglio';

// MAIN
import MainScreen from './component/main/container/mainMain';
import NuovaDicussioneScreen from './component/nuovaDiscussione/container/mainNuovaDiscussione';
import TrovaScreen from './component/trova/container/mainTrova';
import CondividiScreen from './component/condividi/container/mainCondividi';
import CercaScreen from './component/cerca/container/mainCerca';
import TabCS from "./component/CS/container/pageCS";
import TabHome from './component/home/container/mainHome';

// CERCA
import DettaglioSchedaScreen from './component/cerca/component/dettaglioScheda'

// Notification
import Notification from './component/Notification/Notification'

// Bottom Navigator
import { Platform } from 'react-native'
import { createStackNavigator, createSwitchNavigator, createAppContainer } from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';


const AuthStackNavigator = createStackNavigator({
  Welcome: {
    screen: WelcomeScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  Login: {
    screen: LoginScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  SignIn: {
    screen: SignInScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  NewProfilo: {
    screen: NewProfiloScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  SelFilm: {
    screen: SelFilmScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  SelUtenti: {
    screen: SelUtentiScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  SelTipoProfilo: {
    screen: SelTipoProfiloScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  }
});

const RootStack = createStackNavigator({
  Main: {
    screen: MainScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  CS: {
    screen: CSScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  ConceptDettaglio: {
    screen: ConceptDettaglioScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  ProgettoDettaglio: {
    screen: ProgettoDettaglioScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  NuovaDicussione: {
    screen: NuovaDicussioneScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  Trova: {
    screen: TrovaScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  Condividi: {
    screen: CondividiScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  Cerca: {
    screen: CercaScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
  DettaglioScheda: {
    screen: DettaglioSchedaScreen,
    header: null,
    navigationOptions: {
      header: null
    }
  },
});


MainScreen.navigationOptions = {
  tabBarLabel: "Account",
    tabBarOptions: {
      activeTintColor: '#e30009',
      inactiveTintColor: 'light-grey',
    },
  tabBarIcon: ({ tintColor }) => (
    <MaterialCommunityIcons
      name="account"
      style={{ color: tintColor }}
      size={Platform.OS === "ios" ? 28 : 20}
    />
  )
};

TabCS.navigationOptions = {
  tabBarLabel: "Scouting",
    tabBarOptions: {
      activeTintColor: '#e30009',
      inactiveTintColor: 'light-grey',
    },
  tabBarIcon: ({ tintColor }) => (
    <FontAwesome
      name="binoculars"
      style={{ color: tintColor }}
      size={Platform.OS === "ios" ? 28 : 20}
    />
  )
};

TabHome.navigationOptions = {
  tabBarLabel: "Social",
    tabBarOptions: {
      activeTintColor: '#e30009',
      inactiveTintColor: 'light-grey',
    },
  tabBarIcon: ({ tintColor }) => (
    <FontAwesome
      name="globe"
      style={{ color: tintColor }}
      size={Platform.OS === "ios" ? 28 : 20}
    />
  )
};


Notification.navigationOptions = {
  tabBarLabel: "Notifiche",
    tabBarOptions: {
      activeTintColor: '#e30009',
      inactiveTintColor: 'light-grey',
    },
  tabBarIcon: ({ tintColor }) => (
    <Ionicons
      name="ios-flash"
      style={{ color: tintColor }}
      size={Platform.OS === "ios" ? 28 : 20}
    />
  )
};


CondividiScreen.navigationOptions = {
  tabBarLabel: "Scrivi",
    tabBarOptions: {
      activeTintColor: '#e30009',
      inactiveTintColor: 'light-grey',
    },
  tabBarIcon: ({ tintColor }) => (
    <MaterialIcons
      name="edit"
      style={{ color: tintColor }}
      size={Platform.OS === "ios" ? 28 : 20}
    />
  )
};

const App = createBottomTabNavigator(
  {
    Account: { screen: MainScreen },
    Social: { screen: TabHome  },
    Scouting: { screen: TabCS },   
    Notifiche: { screen: Notification  },
    Scrivi: { screen: CondividiScreen },       // For temporary use
  },
);

export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    Auth: AuthStackNavigator,
    App: App,
  },
  {
    initialRouteName: 'App',
  }
));
